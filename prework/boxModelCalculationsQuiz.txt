#div1 {
  height: 150px;
  width: 400px;
  margin: 20px;
  border: 1px solid red;
  padding: 10px;
}

1. Total Height:
    Total Height = margin-top (20px) + border-top (1px) + padding-top (10px) + height of content (150px) + padding-bottom (10px) + border-bottom (1px) + margin-bottom (20px)

    Total Height = 20+1+10+150+10+1+20
    Total Height = 212

2. Total Width:
    Total Width = margin-left (20px) + border-left (1px) + padding-left (10px) + width of content (400px) + padding-right (10px) + border-right (1px) + margin-right (20px)

    Total Width = 20+1+10+400+10+1+20
    Total Width = 462

3. Browser Calculated Height:
    Browser Calculated Height = border-top (1px) + padding-top (10px) + height of content (150px) + padding-bottom (10px) + border-bottom (1px)

    Browser Calculated Height = 1+10+150+10+1
    Browser Calculated Height = 172

4. Browser Calculated Width:
    Browser Calculated Width = border-left (1px) + padding-left (10px) + width of content (400px) + padding-right (10px) + border-right (1px)

    Browser Calculated Width = 1+10+400+10+1
    Browser Calculated Width = 422
