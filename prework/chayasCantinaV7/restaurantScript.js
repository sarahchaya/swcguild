function emailOrPhone(email, phone) {
  return !( email == "" && phone == "" );
}

function otherReason(reason, addInfo) {
  return !( reason == "other" && addInfo == "")
}

function noChecks(boxList) {
  var x = false;
  for ( var i = 0; i < boxList.length; i++) {
    if( boxList[i].checked == true ) {
      x = true;
      break;
    }
  }
  return x;
}

function reqsMet(input) {
  return input == true;
}

function warnings(alertList) {
  var warns = [];
  for (var i = 0; i < alertList.length; i++ ) {
    warns[i] = alertList[i].id;
  }
  return warns;
}
function resetAlerts(alerts) {
  for ( var i = 0; i < alerts.length; i++ ) {
    document.getElementById(alerts[i]).style.display = "none";
  }
}
function createAlerts(inputChecks, alerts) {
  /*commented out code is from original alert popup warning*/
  //var warning = "";
  for ( var i = 0; i < inputChecks.length; i++ ) {
    if ( !inputChecks[i] ) {
      //warning += alerts[i]+" \n \n";
      document.getElementById(alerts[i]).style.display = "block";
    }
  }
  //return warning;
}

document.getElementById("contactUs").addEventListener("submit", function(event) {

  var infoInput = [
    document.getElementById("name").value != "",
    emailOrPhone(document.getElementById("email").value, document.getElementById("phone").value),
    otherReason(document.getElementById("reasons").value, document.getElementById("additionalInfo").value),
    noChecks(document.getElementsByName("contactDays"))
  ]

  var warningDivs = warnings(document.getElementsByClassName("warn"));
  resetAlerts(warningDivs);  //reset warnings so that fixed issues will not show on resubmit (but non-fixed issues still show)

  if( !infoInput.every(reqsMet) ) {

    /*originally coded a standard JavaScript alert, but then upgraded - left in the original code, commented out*/

    /*alert( createAlerts( infoInput, ["Your Name is Required", "Please include either an email address or phone number", "Please include additional information about your reason for inquiry or select a reason from the drop-down.", "Please check at least one day of the week for us to contact you"]) );*/

    document.getElementById("popupContainer").style.display = "block"; //allows popup to show
    createAlerts(infoInput, warningDivs);  //shows individual text warnings by each issue
    event.preventDefault();
  }
});

document.getElementById("ok").addEventListener("click", function(event) {
  document.getElementById("popupContainer").style.display = "none";
});
