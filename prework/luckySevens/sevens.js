var betAmt;
var maxRollCount;
var maxMoney;
var rollsToMax;
var rollCount;
var betAmtMaxRoll;
var betAmtMaxMoney;
var highMoney;
var rollsToHigh;

function setInitialUIState() {
  document.getElementById("startingBet").value = "$0.00";
  document.getElementById("playButton").value = "Play";
}

function betPrompt() {
  document.getElementById("startingBet").value = "$0.00";
  do {
    betAmt = window.prompt("How many dollars would you like to bet?");
    if ( betAmt == null ) {
      document.getElementById("startingBet").value = "$0.00";
      break;
    }
    //Convert any bets typed with $ to plain numbers
    betAmt = betAmt.replace("$","");
    if ( betAmt < 1 ) {
      alert("You must bet at least $1!");
    }
    //distinguish between non-integers and numbers written with .00 after - only whole numbers allowed, but program can convert 3.00 to 3 so everything displays correctly
    if ( betAmt % 1 != 0 ) {
      alert("Bets must be placed in whole dollar amounts")
    } else if ( betAmt.indexOf(".") > -1 ) {
      betAmt = betAmt.slice(0, betAmt.indexOf(".") );
    }
  } while (betAmt == null || betAmt < 1 || betAmt % 1 != 0);
  if ( betAmt != null ) { document.getElementById("startingBet").value = "$"+betAmt+".00";
  }
}

function dieRoll() {
  return Math.floor(Math.random() * 6 + 1);
}

function rollDice() {
  var dieOne = dieRoll();
  var dieTwo = dieRoll();
  return dieOne + dieTwo;
}

function playLuckySevens() {
  //currentBet = 0;
  highMoney = 0;
  rollsToHigh = 0;
  if ( ( betAmt > 0 ) == false ) {
    betPrompt();
  }
  if ( betAmt != null ) {
    var currentBet = betAmt;
    for ( rollCount = 0; currentBet > 0; rollCount++ ) {
      if ( currentBet > highMoney ) {
        highMoney = currentBet;
        rollsToHigh = rollCount;
      }
      var roll = rollDice();
      if ( roll == 7 ) {
        currentBet = currentBet + 4;
      } else {
        currentBet--;
      }
    }

    //track variables throughout session, because I initially thought you had to, and decided to leave it in
    if ( highMoney > maxMoney || maxMoney == null ) {
      maxMoney = highMoney;
      rollsToMax = rollsToHigh;
      betAmtMaxMoney = betAmt;
    }
    if ( rollCount > maxRollCount || maxRollCount == null ) {
      maxRollCount = rollCount;
      betAmtMaxRoll = betAmt;
    }
  }
}

function playing() {
  if ( betAmt == null ) {
    document.getElementById("startingBet").value = "$0.00";
  } else {
    //Wanted to animate the "..." on playing, but couldn't get it to work.  Leaving this in so that it's there if I want to go back to it
    /*var time = (new Date().getTime())+2000;
    for ( time <= new Date(); time++; ) {
      if ( time <= 500 ) {
        document.getElementById("playButton").value=("Playing");
      } else if ( time <= 1000 ) {
          document.getElementById("playButton").value=("Playing.");
        } else if ( time <= 1500 ) {
            document.getElementById("playButton").value=("Playing..");
          } else {
              document.getElementById("playButton").value=("Playing...");
          }
    }*/
    document.getElementById("playButton").value=("Playing...");
  }
}

function playAgain() {
  firstPlay = (maxRollCount > 0);
  document.getElementById("playButton").value = ( firstPlay ? "Play Again" : "Play");

  //display results to the table
  if ( betAmt != null ) {
    document.getElementById("resultsTableContainer").style.display = "block";
    document.getElementById("rStartBet").textContent = "$"+betAmt+".00";
    document.getElementById("rRollCount").textContent = rollCount;
    document.getElementById("rHighMoney").textContent = "$"+highMoney+".00";
    document.getElementById("rRollCountHigh").textContent = rollsToHigh;
    document.getElementById("rMaxMoney").innerHTML = "$"+maxMoney+".00 <br>($"+betAmtMaxMoney+".00 Starting Bet)";
    document.getElementById("rMaxCount").innerHTML = rollsToMax+"<br>($"+betAmtMaxMoney+".00 Starting Bet)";
    document.getElementById("rMaxRollCount").innerHTML = maxRollCount+"<br>($"+betAmtMaxRoll+".00 Starting Bet)";


    //reset starting bet to 0 to offer to play again
    betAmt = 0;
    document.getElementById("startingBet").value = "$"+betAmt+".00";
  }

}
